<?php
/**
 * @file Utility to add existing (drush-site-alias-managed) sites into
 * the Acquia Developer Desktop Control Panel.
 *
 * Built to recover from http://drupal.org/node/1487796#comment-5864296
 *
 * This:
 * - Reads a given site alias
 * - adds appropriate settings to the ini file used by the control panel app.
 *
 * THERE IS MORE TO IT THAN THIS
 * - databases need to be imported properly to the platform.
 * - settings.php need to be updated to include the Acquia auto-generated stuff.
 *
 * Currently this tool just repairs sites that were once already in a managed
 * system and need to be re-registered. It will not (yet) import arbitrary
 * sites from other places. Though it well could.
 */

/**
 * @var Path to the ini file - on my system at least. Should get parameterized.
 */
define('ACQUIA_INI_FILE', '/Applications/acquia-drupal/Acquia Dev Desktop Control Panel.app/Contents/MacOS/dynamic.ini');

/**
 * Implementation of hook_drush_command().
 */
function acquia_drush_command() {
  $items['acquia-absorb'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'description' => 'Absorbs a local site into a local Acquia desktop (control panel) config.',
    'arguments' => array(
      'site-alias' => 'site-alias identifier of the site to grab.',
    ),
    'examples' => array(
      'drush acquia-absorb @site.name' => 'Adds the given site to the Acquia system.',
    ),
    'options' => array(
    ),
  );
  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function acquia_drush_help($section) {
  switch ($section) {
    case 'drush:acquia-absorb':
      return dt('Absorbs a local site into a local Acquia desktop (control panel) config.');
  }
}


/**
 * Absorb the named site into the Acquia settings file.
 *
 * Drush callback;
 * Function magically named from the command definition.
 * drush_HOOK_COMMAND()
 */
function drush_acquia_absorb($site_alias) {

  // Check if control console is already running. That won't work.
  exec('ps -a | grep AcquiaDevDesktopControlPanel', $output);
  // We find our own grep command (twice), so check if there is more than two results.
  if (count($output) > 2) {
    drush_log(dt('AcquiaDevDesktopControlPanel is running. please shut it down before running this or changes cannot be saved. (Quit the application, don\'t just stop it)'), 'error');
    return FALSE;
  }

  $alias_record = drush_sitealias_get_record($site_alias);
  if (empty($alias_record)) {
    drush_log(dt('Failed to find a site-alias entry for %site_alias', array('%site_alias' => $site_alias)), 'error');
    return FALSE;
  }
  // drush_sitealias_get_record Doesn't come with db info by default. Add it.
  #drush_sitealias_add_db_url($alias_record);
  sitealias_get_databases_from_record($alias_record);

  drush_log(dt('Grabbing current console settings from [%ACQUIA_INI_FILE]', array('%ACQUIA_INI_FILE' => ACQUIA_INI_FILE)));
  $backup_filename = ACQUIA_INI_FILE . time();
  file_put_contents($backup_filename, file_get_contents(ACQUIA_INI_FILE));

  $ini_data = parse_ini_file(ACQUIA_INI_FILE, TRUE);

  // ini settings have some funny keys that begin with [sites/m_sites/0]
  // Need to count them to find a new n.
  $new_id = 0;
  foreach ($ini_data as $key => $section_data) {
    if (strstr($key, 'sites/m_sites/') !== FALSE) {
      if ($section_data['host'] == $alias_record['uri']) {
        drush_log(dt('It seems the control console already has an entry for %site_alias - exiting.', array('%site_alias' => $site_alias)), 'error');
        print_r($section_data);
        return FALSE;
      }
      list($sites, $m_sites, $n) = explode('/', $key);
      $new_id = ($n > $new_id) ? $n : $new_id;
    }
  }
  // Use the next available slot.
  $new_id ++;

  drush_log(dt('Absorbing site data from  [%site_alias] site alias', array('%site_alias' => $site_alias)));

  // This array is mostly here for illustration example.
  $section_data_template =  array(
      'host' => 'localhost',
      'dbName' => 'acquia_drupal',
      'drupalSiteDir' => 'default',
      'urlPath' => '',
      'codebaseDir' => '/var/www/acquia-drupal',
      'phpNum' => 10000, # dunno what this does.
  );
  // Convert settings from site-alias into ini data
  $section_data_template['host'] = $alias_record['uri'];
  $section_data_template['dbName'] = $alias_record['databases']['default']['default']['database'];
  $section_data_template['drupalSiteDir'] = drush_sitealias_uri_to_site_dir($alias_record['uri']);
  $section_data_template['codebaseDir'] = $alias_record['root'];

  // Insert the new entry into the ini data
  $ini_data['sites/m_sites/' . $new_id] = $section_data_template;

  drush_log(dt("Rewriting ini settings. New entry [%new_id] for [%uri] added to the console list. Restart the console for it to take effect.", array('%uri' => $alias_record['uri'], '%new_id' => $new_id)), 'success');
  #print_r($section_data_template);
  $ini_rewrite = serialize_ini($ini_data);
  file_put_contents(ACQUIA_INI_FILE, $ini_rewrite);
  // Maybe should check permissions and success here.
  #print_r(get_defined_vars());
}

/**
 * Turn an array into an ini-file-like format.
 *
 * If it were a true ini, it would support quotes,
 * but acquia desktop is not expecting quotes - so don't.
 * @param unknown_type $array
 * @return string
 */
function serialize_ini($array){
  $res = array();
  foreach($array as $key => $val) {
    if(is_array($val)) {
      $res[] = "\n[$key]";
      foreach($val as $skey => $sval) $res[] = "$skey=".(is_numeric($sval) ? $sval : ''.$sval.'');
    }
    else $res[] = "$key = ".(is_numeric($val) ? $val : ''.$val.'');
  }
  return implode("\n", $res);
}

